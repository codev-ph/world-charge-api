<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OTP Verify</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</head>
<script>
    $(function() {
        $("#frm1").submit(function(e) {
            var formData = JSON.stringify({
                otp : $('[name="otp"]').val(),
                mobile_number : $('[name="mobile_number"]').val(),
            });

            const options = {
                method: 'POST',
                body: formData,
                // If you add this, upload won't work
                headers: {
                  'Content-Type': 'application/json',
                    // 'X-Requested-With': 'XMLHttpRequest',
                }
            };

            // delete options.headers['Content-Type'];

            fetch('http://localhost:8009/api/register/via-mobile-number', options);
            
            e.preventDefault();
            return false;
        })
    })
</script>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Upload New File</div>

                    <div class="card-body">
                        <form id="frm1" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="title">Mobile number</label>
                                <input name="mobile_number" class="form-control">

                            </div>
                            <div class="form-group">
                                <label for="title">OTP</label>
                                <input name="otp" class="form-control">
                            </div>
                            <button class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
                @if (session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('success')}}
                </div>
                @endif
            </div>
        </div>
    </div>

</body>

</html>