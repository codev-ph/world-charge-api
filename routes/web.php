<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test-reg', function () {
    return view('registration');
});

Route::get('/otp-verify', function () {
    return view('otp_verify');
});

Route::get('/images', 'ImageController@getImages')->name('images');
Route::post('/upload', 'ImageController@postUpload')->name('uploadfile');
