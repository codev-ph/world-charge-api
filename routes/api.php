<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'API\UserController@details');
    Route::get('/logout', 'API\UserController@logout');
});

Route::post('login/fb', 'API\UserController@fb_login');
Route::post('register/completion', 'API\UserController@register_completion');

Route::post('register/request-otp', 'API\UserController@request_otp');
Route::post('/register/via-mobile-number', 'API\UserController@register_via_number');
