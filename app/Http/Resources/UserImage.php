<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserImage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $image = $this->images->sortByDesc('id')->first();
        return [
            "first_name" => $this->first_name,
            "middle_name" => $this->middle_name,
            "last_name" => $this->last_name,
            "email" => $this->email,
            "mobile_number" => $this->mobile_number,
            "last_logged_in" => $this->last_logged_in,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "image" => $image ? $image->only('url') : [
                "url" => "https://world-charge-cloud.s3-ap-northeast-1.amazonaws.com/profile/default.png"
            ]
        ];
    }
}
