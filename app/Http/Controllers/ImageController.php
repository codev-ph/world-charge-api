<?php

namespace App\Http\Controllers;

use App\Image;
use App\Http\Requests\StoreImage;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    private $image;
    public function __construct(Image $image)
    {
        $this->image = $image;
    }
    public function getImages()
    {
        return view('images')->with('images', auth()->user()->images);
    }
    public function postUpload(StoreImage $request)
    {
        return [$request->file];
        $path = Storage::disk('s3')->put('profile', $request->file);
        $request->merge([
            'size' => $request->file->getClientSize(),
            'path' => $path,
            'user_id' => 1,
        ]);
        $this->image->create($request->only('path', 'title', 'size', 'user_id'));
        return back()->with('success', 'Image Successfully Saved');
    }
}
