<?php

namespace App\Http\Controllers\API;

use App\OneTimePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Requests\StoreUser;
use App\Http\Resources\UserImage;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public $password_grant_client;

    public function __construct()
    {
        // parent::__construct();
        $this->password_grant_client = [
            "id" => "2",
            "secret" => "iFLMxc8cU8iddAff5myVIgbz8OIAXgjrMTHwCQ7j"
        ];
    }
    /** 
     * request_otp api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function request_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
        }

        $user = User::where('mobile_number', $request->mobile_number)->first();
        /* if the mobile number does not exist yet */
        if ($user && $user->last_logged_in) {
            return "Already registered";
        }

        $code = "123456";
        $mobile_number = $request->mobile_number;
        $expiration_date = Carbon::now()->addMinute(1)->format("Y-m-d H:i:s");

        $otp = OneTimePassword::create(
            compact("code", "mobile_number", "expiration_date")
        );
        return $otp;
    }

    /** 
     * register_via_number api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function register_via_number(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'otp' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
        }

        $otp = OneTimePassword::where([
            ['mobile_number', $request->mobile_number],
            ['code', $request->otp],
        ])->orderBy('id', 'DESC')->first();

        if ($otp) {
            /* check the time validity of otp  */
            /* if ($otp->expiration_date < Carbon::now()) {
                return response([
                    "error" => "OTP has expired or invalid.",
                    "d" => $otp
                ], 422);
            } */

            $user = User::where('mobile_number', $request->mobile_number)->first();

            if (!$user) {
                $user = User::create([
                    "mobile_number" => $request->mobile_number
                ]);
                $message = 'Account created successfully! Proceed to the next registration processs.';
                $is_registration_complete = false;
            } else {
                if ($user->email && $user->password) {
                    /**Take note of this: Your user authentication access token is generated here **/
                    $data['token'] =  $this->issueToken([
                        'mobile_number' => $user->mobile_number,
                        'verification_code' => $otp,
                    ], 'phone_verification_code');

                    /*if ($user->images()->latest()->first()) {
                        $data['image'] = $user->images()->latest()->first();
                    } else {
                        $data['image'] = [
                            "url" => "https://world-charge-cloud.s3-ap-northeast-1.amazonaws.com/profile/default.png"
                        ];
                    } */

                    $message = 'Account already exist. An access token is generated.';
                    $is_registration_complete = true;
                } else {
                    $message = 'Registration proccess has not been completed yet.';
                    $is_registration_complete = false;
                }
            }

            $data['user'] = new UserImage($user);
            $status = true;
            return response(compact('data', 'message', 'is_registration_complete', 'status'));
        }

        return response([
            "error" => "OTP is invalid/incorrect",
        ], 404);
    }

    /** 
     * logout api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function logout()
    {
        $user = Auth::user()->token();
        $user->revoke();
        return 'logged out';
    }

    /** 
     * register_completion api 
     * 
     * @return \Illuminate\Http\Response 
     */
    public function register_completion(StoreUser $request)
    {
        /* $validator = Validator::make($request->all(), [
                'mobile_number' => 'required',
                'first_name' => 'required|max:50',
                'middle_name' => 'max:50',
                'last_name' => 'required|max:50',
                'email' => 'required|email|unique:users,email',
                // 'username' => 'required|unique:users,username',
                "password" => 'required',
                "country" => "max:40",
                "city" => "max:30",
            ]);

            if ($validator->fails()) {
                return response(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
            } 
        */

        $user = User::where('mobile_number', $request->mobile_number)->first();

        if (!$user)
            return response(["message" => "Account does not exist.", 'status' => false,],  404);
        elseif ($user && (!$user->username || !$user->password)) {
            $user->first_name = $request->first_name;

            if (isset($user->middle_name))
                $user->middle_name = $request->middle_name;

            $user->last_name = $request->last_name;
            $user->email = $request->email;
            // $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->country = $request->country;
            $user->city = $request->city;
            $user->update();
            if (isset($request->profile_image))
                $image = $this->storeImage($user, $request->profile_image);
            /* else {
                $image = [
                    "url" => "https://world-charge-cloud.s3-ap-northeast-1.amazonaws.com/profile/default.png"
                ];
            } */

            $message = "Registration has been completed.";
            $data['token'] =  $this->issueToken([
                'username' => $user->email,
                'password' => $request->password,
            ]);
        } else {
            $message = "Registration has been already completed. No changes where made.";
            $data['token'] =  $user->createToken('WCA');
        }

        $data['user'] =  new UserImage($user);

        $is_registration_complete = true;
        $status = true;

        return response(compact('data', 'message', 'is_registration_complete', 'status'));
    }

    public function fb_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'access_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
        }

        $data['token'] =  $this->issueToken(
            [
                "token" => $request->access_token,
                'provider' => 'facebook'
            ],
            'social'
        );

        return $data;
    }

    private function issueToken($user, $grant_type = 'password')
    {
        // Close request to clear up some resources curl_close($ch);
        $http = new \GuzzleHttp\Client;

        $url = config('services.server_oauth.url') == "default" ? route('passport.token') : config('services.server_oauth.url');

        $params  = [
            'grant_type' => $grant_type,
            'client_id' => config('services.server_oauth.client_id'),
            'client_secret' => config('services.server_oauth.client_secret'),
            'scope' => '',
        ];
        // return $grant_type;
        if ($grant_type == 'password') {
            $params['username'] =  $user['username'];
            $params['password'] = $user['password'];
        } else if ($grant_type == 'phone_verification_code') {
            $params['phone_number'] =  $user['mobile_number'];
            $params['verification_code'] =  $user['verification_code'];
        } else if ($grant_type == 'social') {
            $params['provider'] = $user['provider'];
            $params['access_token'] = $user['token'];
        }

        // return $params;
        $response = $http->post($url, [
            'form_params' => $params,
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    public function storeImage($user, $profile_image)
    {
        $file_name = $user->id . "." . $profile_image->getClientOriginalExtension();
        $path = Storage::disk('s3')->putFileAs('profile', $profile_image, $file_name, 'public');
        $image = $user->images()->create([
            'size' => $profile_image->getClientSize(),
            'path' => $path,
        ]);
        return $image;
    }
}
