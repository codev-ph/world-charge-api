<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile_number' => 'required',
            'first_name' => 'required|max:50',
            'middle_name' => 'max:50',
            'last_name' => 'required|max:50',
            'email' => 'required|email|unique:users,email',
            // 'username' => 'required|unique:users,username',
            "password" => 'required',
            'profile_image' => 'image|max:2000',
            // "country" => "max:40",
            // "city" => "max:30",
        ];
    }
}
