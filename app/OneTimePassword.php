<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneTimePassword extends Model
{
    protected $fillable = [
        'code', 'mobile_number', 'expiration_date'
    ];

    public $timestamps = false;
}
